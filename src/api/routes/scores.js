const express = require('express');
const router = express.Router();

const pouchdb = require('pouchdb');

var database = new pouchdb('scores');

// Get top 10 from leaderboard
router.get('/', (req, res, next)=>{
    database.allDocs({include_docs: true})
    .then((result) => {
        if (result.total_rows <= 0) {
            res.status(200).json({});
            return;
        }

        let rows = result.rows;
        rows.sort((left, right) => {
            if (left.doc.score > right.doc.score) {
                return -1;
            }
            else if (left.doc.score < right.doc.score) {
                return 1;
            }

            return 0;
        })

        let scores = [];
        for (let i = 0; i < Math.min(result.total_rows, 10); ++i) {
            scores.push({
                username: rows[i].id,
                score: rows[i].doc.score
            });
        }

        res.status(200).json({
            scores
        });
    });
});

// Get specific user's score
router.get('/:username', (req, res, next) => {
    const username = req.params.username;

    database.get(username)
    .catch((err)=>{
        if (err.name === 'not_found') {
            return {
                _id: username,
                score: 0
            }
        }
        else {
            throw err;
        }
    })
    .then((doc) => {
        res.status(200).json({
            username: doc._id,
            score: doc.score
        });
    })
    .catch((err) => {
        
        console.log(err);
    });
});

// Delete a user's score
router.delete('/:username', (req, res, next) => {
    const username = req.params.username;

    database.get(username)
    .catch((err)=>{
        if (err.name === 'not_found') {
            return null;
        }
        else {
            throw err;
        }
    })
    .then((doc) => {
        if (doc) {
            return database.remove(doc);
        }
        else {
            return { ok: false }
        }
    })
    .then((result) => {
        res.status(200).json({
            deleted: result.ok,
            username: username
        });
    })
    .catch((err) => {
        throw err;
    });
});

// Insert a user's score
router.post('/', (req, res, next)=>{
    const user = {
        username: req.body.username,
        score: req.body.score
    };

    console.log(user.username + " " + user.score);

    database.get(user.username)
    .catch((err)=>{
        if (err.name === 'not_found') {
            return {
                _id: user.username,
                score: 0
            }
        }
        else {
            throw err;
        }
    })
    .then((doc) => {
        if (doc.score < user.score) {
            let prevScore = doc.score;
            doc.score = user.score;

            database.put(doc)
            .then((doc) => {
                if (doc.ok) {
                    res.status(200).json({
                        updated: true,
                        score: user,
                        previousScore: prevScore
                    });
                }
            })
            .catch((err) => {
                throw err;
            });
        }
        else {
            res.status(200).json({
                updated: false
            });
        }
    })
    .catch((err) => {
        throw err;
    });
});

module.exports = router;