const express = require('express');
const app = express();
const bodyParser = require('body-parser');

const scoreRoutes = require('./api/routes/scores');

app.use(bodyParser.urlencoded({extended: false}));
app.use(bodyParser.json());

app.use(((req, res, next) => {
    res.header('Access-Control-Allow-Origin', '*');
    res.header('Access-Control-Allow-Headers',
    'Origin, X-Requested-With, Content-Type, Accept, Authorization');
    if (req.method === 'OPTIONS') {
        res.header('Access-Control-Allow-Methods', 
        'PUT, POST, PATCH, DELETE, GET');
        return res.status(200).json({});
    }
    next();
}));

// Routes
app.use('/scores', scoreRoutes);

// Error handling
app.use((req, res, next) => {
    const error = new Error('Not Found');
    error.status = 404;
    next(error);
});

app.use((err, req, res, next) => {
    res.status = err.status || 500;
    res.json({
        error: {
            message: err.message
        }
    });
});

module.exports = app;