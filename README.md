# Jalloo 2018 Leaderboard Server

### Pre-requisites
- [NodeJS](https://nodejs.org/en/)
- [NPM](https://www.npmjs.com/package/npm)

### Initialize Dependencies
Use `npm install` to install the required dependencies.
##### Local
- [Express](https://expressjs.com/)
- [Body-Parser](https://www.npmjs.com/package/body-parser)
- [PouchDB](https://pouchdb.com/)

##### Development
- [Nodemon](https://nodemon.io/)

### Start Server
Use `npm start` to start the server.

### API
Use `GET http://localhost:3000/scores` to get the top scores in the leaderboard.
```json
{
  "records": [
    {
      "username": "user1",
      "score": 10
    },
    {
      "username": "user2",
      "score": 5
    }
  ]
}
```

Use `GET http://localhost:3000/scores/user1` to get `user1`'s score
```json
{
  "id": "user1",
  "score": 10
}
```

Use `DELETE http://localhost:3000/scores/user1` to delete `user1`'s record
```json
{
  "deleted": true,
  "username": "user1"
}
```

Use `POST http://localhost:3000/scores` to send/update a score
```json
{
    "name": "user2",
    "score": 120
}
```

returns
```json
{
  "updated": true,
  "record": {
    "name": "user2",
    "score": 120
  },
  "previousScore": 12
}
```


